use std::time::{
    Duration,
    SystemTime,
    SystemTimeError,
    UNIX_EPOCH,
};

pub mod duration_ext;

pub trait SystemTimeExt {
    fn duration_since_epoch(&self) -> Result<Duration, SystemTimeError>;

    #[inline]
    fn seconds_since_epoch(&self) -> Result<u64, SystemTimeError> {
        self.duration_since_epoch().map(|d| d.as_secs())
    }
}

impl SystemTimeExt for SystemTime {
    #[inline]
    fn duration_since_epoch(&self) -> Result<Duration, SystemTimeError> {
        self.duration_since(UNIX_EPOCH)
    }
}

pub trait DurationExt {
    fn as_seconds_since_epoch(&self) -> SystemTime;
}

impl DurationExt for Duration {
    #[inline]
    fn as_seconds_since_epoch(&self) -> SystemTime {
        UNIX_EPOCH + *self
    }
}
