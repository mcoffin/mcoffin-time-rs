use std::time::Duration;

pub const fn from_days(days: u64) -> Duration {
    Duration::from_secs(days * 24 * 60 * 60)
}

pub const fn from_hours(hours: u64) -> Duration {
    Duration::from_secs(hours * 60 * 60)
}

pub const fn from_mins(mins: u64) -> Duration {
    Duration::from_secs(mins * 60)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_days_works() {
        assert_eq!(from_days(7), Duration::from_secs(7 * 24 * 60 * 60));
    }

    #[test]
    fn from_hours_works() {
        assert_eq!(from_hours(10), Duration::from_secs(10 * 60 * 60));
    }

    #[test]
    fn from_mins_works() {
        assert_eq!(from_mins(10), Duration::from_secs(10 * 60));
    }

    #[test]
    fn all_match() {
        assert_eq!(from_days(2), from_hours(2 * 24));
        assert_eq!(from_hours(2), from_mins(2 * 60));
        assert_eq!(from_mins(2), Duration::from_secs(2 * 60));
    }
}
